From 15edd889b7bd409f063bcd75bf933a1b182ab602 Mon Sep 17 00:00:00 2001
From: Simon Chopin <chopin.simon@gmail.com>
Date: Wed, 12 Jun 2013 09:16:27 +0200
Subject: Fix unspecified except: blocks

 Those are bad since they catch every exception out there, including
 KeyboardInterrupt and the like.
 .
 The first block is entirely removed as the two function call are not
 supposed to raise anything given legit parameters.
 The second should, according to the method description, only catch
 KeyError to change them into AttributeError.
Forwarded: https://github.com/dsc/bunch/pull/9
Patch-Name: fix-except
---
 bunch/__init__.py | 7 ++-----
 1 file changed, 2 insertions(+), 5 deletions(-)

diff --git a/bunch/__init__.py b/bunch/__init__.py
index d665004..bfdb512 100644
--- a/bunch/__init__.py
+++ b/bunch/__init__.py
@@ -82,10 +82,7 @@ class Bunch(dict):
             >>> 'hello' in b
             True
         """
-        try:
-            return hasattr(self, k) or dict.__contains__(self, k)
-        except:
-            return False
+        return hasattr(self, k) or dict.__contains__(self, k)
     
     # only called if k not found in normal places 
     def __getattr__(self, k):
@@ -142,7 +139,7 @@ class Bunch(dict):
         except AttributeError:
             try:
                 self[k] = v
-            except:
+            except KeyError:
                 raise AttributeError(k)
         else:
             object.__setattr__(self, k, v)
